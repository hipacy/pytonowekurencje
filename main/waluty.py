import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import messagebox
import requests
from urllib3.exceptions import NewConnectionError


class AppEngine():

    def __init__(self):
        self.app = tk.Tk()
        self.app.geometry('350x180')
        self.app.resizable(False, False)
        self.app.title('Przelicznik walut')
        self.currencies = ["USD", "EUR", "GBP", "CHF", "RUB", "TRY", "CNY", "PLN"]
        self.currenciesMap = {"USDEUR": "0.87190", "USDGBP": "0.77823", "USDCHF": "0.98394", "USDRUB": "66.9492",
                              "USDTRY": "5.47013", "USDCNY": "6.73979", "USDPLN": "3.74240",
                              "EURUSD": "1.14688", "EURGBP": "0.89257", "EURCHF": "1.12853", "EURRUB": "76.8400",
                              "EURTRY": "6.27163", "EURCNY": "7.76304 ", "EURPLN": "4.29226",
                              "GBPUSD": "1.28486", "GBPEUR": "1.12043", "GBPCHF": "1.26437", "GBPRUB": "85.8767",
                              "GBPTRY": "7.03072", "GBPCNY": "8.59470", "GBPPLN": "4.80901",
                              "CHFUSD": "1.01632", "CHFEUR": "0.88620", "CHFGBP": "0.79100", "CHFRUB": "68.1287",
                              "CHFTRY": "5.56186", "CHFCNY": "6.85802", "CHFPLN": "3.80389",
                              "RUBUSD": "1.49345", "RUBEUR": "1.30120", "RUBGBP": "1.16446", "RUBCHF": "1.46781",
                              "RUBTRY": "8.19194", "RUBCNY": "0.10063", "RUBPLN": "0.05588",
                              "TRYUSD": "0.18273", "TRYEUR": "0.15937", "TRYGBP": "0.14224", "TRYRUB": "12.2071",
                              "TRYCNY": "1.24007", "TRYPLN": "0.68407", "TRYCHF": "0.17974",
                              "CNYUSD": "0.14837", "CNYEUR": "12.8808", "CNYGBP": "11.6351", "CNYRUB": "9.93707",
                              "CNYTRY": "0.80641", "CNYPLN": "0.55329", "CNYCHF": "0.14581",
                              "PLNUSD": "0.26719", "PLNEUR": "0.23298", "PLNGBP": "0.20794", "PLNRUB": "17.8970",
                              "PLNTRY": "1.46178", "PLNCNY": "1.80745", "PLNCHF": "0.26288"}

        self.wynik = 0

        self.combo1 = ttk.Combobox(self.app, values=self.currencies, state="readonly")
        self.combo1.grid(column=0, row=1)

        self.combo2 = ttk.Combobox(self.app, values=self.currencies, state="readonly")
        self.combo2.grid(column=0, row=3)

        self.wynik = Entry()
        self.wynik.grid(row=3, column=2, columnspan=3, sticky=W)

        self.kwotaWejsciowa = Entry()
        self.kwotaWejsciowa.grid(row=1, column=2, columnspan=3, sticky=W)

        self.chkboxMode = BooleanVar()
        self.bool = IntVar()
        self.bool.set(0)

        tk.Label(self.app, text="Wybierz pierwszą walutę:").grid(column=0, row=0)
        tk.Label(self.app, text="Wybierz drugą walutę").grid(column=0, row=2)

        button = Button(font=("Tahoma", 10, "bold"), background="lightblue", text="Przelicz!", width=14)
        button.grid(row=8, column=0, sticky=W)
        button["command"] = self.calculate

        tk.Label(self.app, text="Podaj kwotę:").grid(column=2, row=0)
        tk.Label(self.app, text="Po przeliczniu:").grid(column=2, row=2)

        Checkbutton(self.app, text="Użyj REST (api.exchangeratesapi.io)", variable=self.chkboxMode,
                    command=self.setBool).grid(row=7, sticky=W)

    def getTkApp(self):
        return self.app

    def setBool(self):
        if self.bool.get() == 0:
            self.bool.set(1)
        else:
            self.bool.set(0)

    def calculateConventional(self):
        try:
            result = float(float(self.kwotaWejsciowa.get()) * float(
                self.currenciesMap[str(self.combo1.get()) + self.combo2.get()]))
        except:
            messagebox.showinfo("BŁĄD", "Podaj poprawną kwotę i walute!!!")
        else:
            self.wynik.insert(0, result)
            print()

    def calculateRest(self):
        try:
            r = requests.get(
                url="https://api.exchangeratesapi.io/latest?base=%s&symbols=%s" % (
                    self.combo1.get(), self.combo2.get()))

        except:
            messagebox.showinfo("BLĄD", "Brak połączenia!")

        else:
            print(str(r.json()))
            print(str(r.json()["rates"][self.combo2.get()]))

            try:
                result = float(r.json()["rates"][self.combo2.get()]) * float(self.kwotaWejsciowa.get())
            except:
                messagebox.showinfo("BŁĄD", "Podaj poprawną kwotę i walute!!!")
            else:
                self.wynik.insert(0, result)

    def calculate(self):
        self.wynik.delete(0, END)
        if self.bool.get() == 0:
            print("Używam konwencjonalnej konwersji!")
            self.calculateConventional()
        else:
            print("Używam interfejsu rest!")
            self.calculateRest()


appEnginge = AppEngine()
appEnginge.app.mainloop()
